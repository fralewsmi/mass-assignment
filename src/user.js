// A psudo authenticator 
// returns authenticated user ID
function auth_user_id() {
    return 2;
}

class UserClass {
    constructor(name, email, role) {
        this.id = auth_user_id();
        this.name = new Name(name).value;
        this.email = new Email(email).value;
        this.role = role
    }

    setName(name) {
        this.name = new Name(name).value
    }

    setEmail(email) {
        this.email = new Email(email).value
    }
}

class Name {
    constructor(value) {
        if (typeof value !== 'string') {
            throw new TypeError()
        } else {
            this.value = value
        }
        Object.freeze(this);
    }
}

class Email {
    constructor(value) {
        if (typeof value !== 'string') {
            throw new TypeError()
        } else {
            this.value = value
        }
        Object.freeze(this);
    }
}

// User data model
var User = new UserClass("", "", "user")

module.exports = { User, UserClass, Name, Email, auth_user_id };
