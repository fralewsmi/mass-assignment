const request = require('supertest');
const app = require('./app');

describe('usability', () => {

    it('when request /status, should return 200', async () => {
        const res = await request(app)
            .get('/status');
        expect(res.statusCode).toEqual(200);
    });

    it('Request to store user email and name, should return 200 with success message', async () => {
        const res = await request(app)
            .post('/')
            .send('{"email": "foo@bar.com", "name": "Foo Bar"}')
            .set('Content-Type', 'application/json');
        expect(res.statusCode).toEqual(200);
        expect(res.text).toContain('Your preferences have been successfully saved');
    });

    it('Request to store user pref without email, should return 400 with an error message', async () => {
        const res = await request(app)
            .post('/')
            .send('{"name": "Foo Bar"}')
            .set('Content-Type', 'application/json');
        expect(res.statusCode).toEqual(400);
        expect(res.text).toContain('Please provide both email and name');
    });


    it('Request to store user pref without name, should return 400 with an error message', async () => {
        const res = await request(app)
            .post('/')
            .send('{"email": "foo@bar.com"}')
            .set('Content-Type', 'application/json');
        expect(res.statusCode).toEqual(400);
        expect(res.text).toContain('Please provide both email and name');
    });
});
