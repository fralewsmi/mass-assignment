const request = require('supertest');
const app = require('./app');


describe('security', () => {

    it('Request to store user role, should return 400', async () => {
        const res = await request(app)
            .post('/')
            .send('{"name": "Foo Bar", "email": "foo@bar.com", "role": "admin"}')
            .set('Content-Type', 'application/json');
        expect(res.statusCode).toEqual(400);
    });

    it('Request to store user id, should return 400', async () => {
        const res = await request(app)
            .post('/')
            .send('{"name": "Foo Bar", "email": "foo@bar.com", "id": 1}')
            .set('Content-Type', 'application/json');
        expect(res.statusCode).toEqual(400);
    });
 
    it('Request to add new User property, should return 400', async () => {
        const res = await request(app)
            .post('/')
            .send('{"name": "Foo Bar", "email": "foo@bar.com", "foo": "bar"}')
            .set('Content-Type', 'application/json');
        expect(res.statusCode).toEqual(400);
    });   
});
